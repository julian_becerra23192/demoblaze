Feature: demoblaze compra de lapto,
  yo como usuario de la pagina web,
  requiero iniciar seccion,
  y comprar una lapto


  Scenario Outline: yo como usuario entro
    Given a la pagina web demoblaze
    When usuario va a la seccion laptop y compra una laptop
      | nombre   | pais   | ciudad   | tarjeta_credito   | mes   | anio   |
      | <nombre> | <pais> | <ciudad> | <tarjeta_credito> | <mes> | <anio> |
    Then el usuario visualiza el mensaje de compra

    Examples:
      | nombre | pais     | ciudad     | tarjeta_credito | mes   | anio |
      | julian | colombia | mededellin | 14512           | 10    | 2021 |
      | 23232  | 32323    | 32323      | holajulian      | he777 | 899  |
      |        |          |            | holajulian      |       |      |
      | julian |          |            | holajulian      |       |      |


