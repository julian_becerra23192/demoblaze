Feature: demoblaze inscripcion de usuarios,
  yo como usuario de la pagina web,
  requiero crear la suscripcion en la pagina,
  para poder inicar seccion


  Scenario Outline: yo como usuario entro
    Given a la pagina web demoblaze
    When usuario se suscribe en la pagina web
      | usuario   | contrasenia   |
      | <usuario> | <contrasenia> |
    Then el usuario visualiza Sign up successful

    Examples:
      | usuario                                                                                    | contrasenia |
      | juli18                                                                                     | sss         |
      | julian                                                                                     |             |
      | julianjulianjulianjulianjulianjulianjulianjulianjulianjulianjulianjulianjulianjulianjulian | sss         |



