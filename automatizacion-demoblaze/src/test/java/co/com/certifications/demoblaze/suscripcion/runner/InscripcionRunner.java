package co.com.certifications.demoblaze.suscripcion.runner;


import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/inscribirse_demoblaze.feature",
        glue = "co.com.certifications.demoblaze.suscripcion.stepdefinitions",
        snippets = SnippetType.CAMELCASE)

public class InscripcionRunner {
}
