package co.com.certifications.demoblaze.suscripcion.stepdefinitions;

import co.com.demoblaze.models.Persona;
import co.com.demoblaze.question.VisualizarMensaje;
import co.com.demoblaze.task.InscripcionUsuario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import java.util.List;

import static co.com.demoblaze.constans.Constans.DEMOBLAZE;
import static co.com.demoblaze.constans.Constans.JULIAN;

public class InscripcionStepDefinitions {

    @Before
    public void preparation() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled(JULIAN);
    }

    @Given("^a la pagina web demoblaze$")
    public void aLaPaginaWebDemoblaze() {
        OnStage.theActorInTheSpotlight().wasAbleTo(Open.url(DEMOBLAZE));
    }


    @When("^usuario se suscribe en la pagina web$")
    public void usuarioSeSuscribeEnLaPaginaWeb(List<Persona> personas) {
        OnStage.theActorInTheSpotlight().attemptsTo(InscripcionUsuario.elUsuarioSeInscribe(personas.get(0)));
    }

    @Then("^el usuario visualiza Sign up successful$")
    public void elUsuarioVisualizaSignUpSuccessful() {



    }


}
