package co.com.certifications.demoblaze.suscripcion.stepdefinitions;

import co.com.demoblaze.models.CompraLaptop;
import co.com.demoblaze.question.VisualizaMensajeDeCompra;
import co.com.demoblaze.task.ComprarLaptopIon;
import co.com.demoblaze.task.VisualizarLaptos;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.Matchers;

import java.util.List;

public class CompraLaptopStepDefinitions {





    @When("^usuario va a la seccion laptop y compra una laptop$")
    public void usuarioVaALaSeccionLaptopYCompraUnaLaptop(List<CompraLaptop> compraLaptop) {
        OnStage.theActorInTheSpotlight().attemptsTo(VisualizarLaptos.elUsuarioVeLaptos());
        OnStage.theActorInTheSpotlight().attemptsTo(ComprarLaptopIon.elUsuarioCompraLaptop(compraLaptop.get(0)));
    }

    @Then("^el usuario visualiza el mensaje de compra$")
    public void elUsuarioVisualizaElMensajeDeCompra() {

        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(
                VisualizaMensajeDeCompra.MensajeDeCompra(),
                Matchers.containsString("Thank you for your purchase!")));

    }
}
