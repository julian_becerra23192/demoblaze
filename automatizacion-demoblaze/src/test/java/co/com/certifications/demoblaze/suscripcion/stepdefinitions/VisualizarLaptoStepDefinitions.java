package co.com.certifications.demoblaze.suscripcion.stepdefinitions;

import co.com.demoblaze.question.VisualizaMensajeDeCompra;
import co.com.demoblaze.question.VisualizarMensaje;
import co.com.demoblaze.question.VisualizoPrecioLaptop;
import co.com.demoblaze.task.VisualizarLaptos;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import static co.com.demoblaze.constans.Constans.DEMOBLAZE;
import static co.com.demoblaze.constans.Constans.JULIAN;

public class VisualizarLaptoStepDefinitions {

    @Before
    public void preparation() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled(JULIAN);
    }

    @Given("^web demoblaze$")
    public void webDemoblaze() {
        OnStage.theActorInTheSpotlight().wasAbleTo(Open.url(DEMOBLAZE));
    }

    @When("^inicio seccion voy a la seccion de laptos$")
    public void inicioSeccionVoyALaSeccionDeLaptos() {
        OnStage.theActorInTheSpotlight().attemptsTo(VisualizarLaptos.elUsuarioVeLaptos());
    }



    @Then("^visualizo precio de la lapto sonyvai$")
    public void visualizoPrecioDeLaLaptoSonyvai() {

        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(
                VisualizoPrecioLaptop.visualizaLapto(),
                Matchers.containsString("$790")));

    }




}
