package co.com.demoblaze.question;

import co.com.demoblaze.constans.Alerta;
import co.com.demoblaze.constans.espera.AlertaUn;
import co.com.demoblaze.userinterfaces.CompraItem;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class VisualizaMensajeDeCompra implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
       // System.out.println((AlertaUn.alerta()));
        System.out.println(Text.of(CompraItem.MENSAJE_FINALIZAR_COMPRA).viewedBy(actor).asString());
        return Text.of(CompraItem.MENSAJE_FINALIZAR_COMPRA).viewedBy(actor).asString();
    }
    public static VisualizaMensajeDeCompra MensajeDeCompra() {

        return new VisualizaMensajeDeCompra();
    }
}
