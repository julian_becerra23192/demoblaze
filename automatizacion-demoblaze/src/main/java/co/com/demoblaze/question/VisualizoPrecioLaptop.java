package co.com.demoblaze.question;

import co.com.demoblaze.userinterfaces.CompraItem;
import co.com.demoblaze.userinterfaces.Laptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class VisualizoPrecioLaptop implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {

        System.out.println(Text.of(Laptops.LAPTOPS).viewedBy(actor).asString());
        return Text.of(Laptops.LAPTOPS).viewedBy(actor).asString();
    }
    public static VisualizoPrecioLaptop visualizaLapto() {

        return new VisualizoPrecioLaptop();
    }
}
