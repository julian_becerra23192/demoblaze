package co.com.demoblaze.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.questions.Text;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

public class VisualizarMensaje implements Question<String> {



    @Override
    public String answeredBy(Actor actor) {
        WebDriver driver =  BrowseTheWeb.as(actor).getDriver();
        Alert alert = driver.switchTo().alert();
        return Text.of(alert.getText()).viewedBy(actor).asString();
    }

public static VisualizarMensaje SignUpSuccessful(){

        return new VisualizarMensaje();
}

}
