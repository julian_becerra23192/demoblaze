package co.com.demoblaze.task;

import co.com.demoblaze.constans.Alerta;
import co.com.demoblaze.constans.espera.Esperar;
import co.com.demoblaze.models.Persona;
import co.com.demoblaze.userinterfaces.InicioDemoBlaze;
import co.com.demoblaze.userinterfaces.Laptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;


public class VisualizarLaptos implements Task {



    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                Click.on(InicioDemoBlaze.CLICK_LOGIN),
                Esperar.unTiempo(3000),
                SendKeys.of("juli18").into(InicioDemoBlaze.DIGITA_USUARIO_LOGIN),
                Esperar.unTiempo(1000),
                SendKeys.of("sss").into(InicioDemoBlaze.DIGITA_CONTRASENIA_LOGIN),
                Esperar.unTiempo(1000),
                Click.on(InicioDemoBlaze.CLICK_LOG_IN),
                Esperar.unTiempo(1000),
                Click.on(InicioDemoBlaze.CLICK_LAPTOPS),
                Esperar.unTiempo(2000)





        );

    }
    public static VisualizarLaptos elUsuarioVeLaptos(){

        return Tasks.instrumented(VisualizarLaptos.class);
    }
}
