package co.com.demoblaze.task;

import co.com.demoblaze.constans.Alerta;
import co.com.demoblaze.constans.espera.Esperar;
import co.com.demoblaze.models.Persona;
import co.com.demoblaze.userinterfaces.InicioDemoBlaze;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;


public class InscripcionUsuario implements Task {
    private Persona persona;

    public InscripcionUsuario(Persona persona) {
        this.persona = persona;

    }




    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Click.on(InicioDemoBlaze.CLICK_INSCRIBIRSE),
                SendKeys.of(persona.getUsuario()).into(InicioDemoBlaze.DIGITAR_USUARIO),
                SendKeys.of(persona.getContrasenia()).into(InicioDemoBlaze.DIGITAR_CONTRASENIA),
                Click.on(InicioDemoBlaze.CLICK_EN_INSCRIPCION),
                Esperar.unTiempo(3000),
                Alerta.alerta(),
                Esperar.unTiempo(3000)




        );

    }
    public static InscripcionUsuario elUsuarioSeInscribe(Persona persona){


        return Tasks.instrumented(InscripcionUsuario.class,persona);
    }
}
