package co.com.demoblaze.task;

import co.com.demoblaze.constans.Alerta;
import co.com.demoblaze.constans.espera.Esperar;
import co.com.demoblaze.models.CompraLaptop;
import co.com.demoblaze.userinterfaces.CompraItem;
import co.com.demoblaze.userinterfaces.Laptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SendKeys;

public class ComprarLaptopIon implements Task {

    private CompraLaptop comprarLaptop;

    public ComprarLaptopIon(CompraLaptop comprarLaptop) {
        this.comprarLaptop = comprarLaptop;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Esperar.unTiempo(2000),
                Click.on(Laptops.LAPTOP_SONY_VAIO_5),
                Esperar.unTiempo(2000),
                Click.on(Laptops.LAPTOP_COMPRA),
                Esperar.unTiempo(2000),
                Alerta.alerta(),
                Esperar.unTiempo(2000),
                Click.on(Laptops.CARRITO_DE_COMPRA),
                Esperar.unTiempo(2000),
                Click.on(CompraItem.COMPRA),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getNombre()).into(CompraItem.NOMBRE),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getPais()).into(CompraItem.PAIS),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getCiudad()).into(CompraItem.CIUDAD),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getTarjeta_credito()).into(CompraItem.TARJETA_CREDITO),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getMes()).into(CompraItem.MES),
                Esperar.unTiempo(2000),
                SendKeys.of(comprarLaptop.getAnio()).into(CompraItem.ANIO),
                Esperar.unTiempo(2000),
                Click.on(CompraItem.FINALIZAR_COMPRA),
                Esperar.unTiempo(2000),
                Scroll.to(CompraItem.PRODUCT).andAlignToTop()



                        );
    }
    public static ComprarLaptopIon elUsuarioCompraLaptop(CompraLaptop comprarLaptop) {

        return Tasks.instrumented(ComprarLaptopIon.class,comprarLaptop);
    }
}
