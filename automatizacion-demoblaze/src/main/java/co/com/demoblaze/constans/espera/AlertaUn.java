package co.com.demoblaze.constans.espera;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;

public class AlertaUn implements Interaction {


    @Override
    public <T extends Actor> void performAs(T actor) {
        WebDriver driver = BrowseTheWeb.as(actor).getDriver();
        org.openqa.selenium.Alert alert = driver.switchTo().alert();
        alert.getText();
    }

    public static co.com.demoblaze.constans.Alerta alerta() {


        return Tasks.instrumented(co.com.demoblaze.constans.Alerta.class);


    }
}