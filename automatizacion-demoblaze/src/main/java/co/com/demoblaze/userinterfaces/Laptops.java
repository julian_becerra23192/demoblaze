package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Laptops {

    public static final Target LAPTOPS=
            Target.the("laptop sony vaio i5").
                    located(By.xpath("(//h5[contains(text(),'$790')])[1]"));

    public static final Target LAPTOP_SONY_VAIO_5=
            Target.the("laptop sony vaio i5").
                    located(By.xpath("//A[contains(text(),'Sony vaio i5')]"));

    public static final Target LAPTOP_COMPRA=
            Target.the("compra de laptop sony vaio i5").
                    located(By.xpath("//A[contains(text(),'Add to cart')]"));

    public static final Target CARRITO_DE_COMPRA=
            Target.the("ir al carrito de compra").
                    located(By.xpath("//a[@id='cartur']"));








}
