package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class InicioDemoBlaze {
    public static final Target CLICK_INSCRIBIRSE=
            Target.the("click en inscribirse").
                    located(By.id("signin2"));

    public static final Target DIGITAR_USUARIO=
            Target.the("digitar usuario ").
                    located(By.id("sign-username"));

    public static final Target DIGITAR_CONTRASENIA=
            Target.the("digitar contrasenia").
                    located(By.id("sign-password"));

    public static final Target CLICK_EN_INSCRIPCION=
            Target.the("finalizar inscripcion").
                    located(By.xpath("(//button[@class='btn btn-primary'])[2]"));

    public static final Target CLICK_LOGIN=
            Target.the("click en el boton loggin").
                    located(By.id("login2"));

    public static final Target DIGITA_USUARIO_LOGIN=
            Target.the("digita el usuario").
                    located(By.id("loginusername"));

    public static final Target DIGITA_CONTRASENIA_LOGIN=
            Target.the("digita la contrasenia").
                    located(By.id("loginpassword"));

    public static final Target CLICK_LOG_IN=
            Target.the("click en log in").
                    located(By.xpath("//button[@onclick='logIn()']"));

    public static final Target CLICK_LAPTOPS=
            Target.the("click en categoria laptops").
                    located(By.xpath("//a[contains(text(),'Laptops')]"));




}
