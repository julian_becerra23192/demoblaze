package co.com.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CompraItem {


    public static final Target COMPRA=
            Target.the("iniciar proceso de compra").
                    located(By.xpath("//button[contains(text(),'Place Order')]"));

    public static final Target NOMBRE=
            Target.the("digitar nombre").
                    located(By.id("name"));

    public static final Target PAIS=
            Target.the("digitar pais ").
                    located(By.id("country"));

    public static final Target CIUDAD=
            Target.the("digitar ciudad").
                    located(By.id("city"));

    public static final Target TARJETA_CREDITO=
            Target.the("digitar tarjeta de credito").
                    located(By.id("card"));

    public static final Target MES=
            Target.the("digitar mes de la tarjeta de credito").
                    located(By.id("month"));


    public static final Target ANIO=
            Target.the("digitar anio de la tarjeta de credito").
                    located(By.id("year"));

    public static final Target FINALIZAR_COMPRA=
            Target.the("finalizar compra").
                    located(By.xpath("//button[contains(text(),'Purchase')]"));


    public static final Target MENSAJE_FINALIZAR_COMPRA=
            Target.the("finalizar compra").
                    located(By.xpath("//h2[contains(text(),'Thank you for your purchase!')]"));

    public static final Target PRODUCT=
            Target.the("finalizar compra").
                    located(By.xpath("(//h2)[1]"));





}
