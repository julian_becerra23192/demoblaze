package co.com.demoblaze.models;

public class CompraLaptop {
    private String nombre;
    private String pais;
    private String ciudad;
    private String tarjeta_credito;
    private String mes;
    private String anio;

    public CompraLaptop(String nombre, String pais, String ciudad, String tarjeta_credito, String mes, String anio) {
        this.nombre = nombre;
        this.pais = pais;
        this.ciudad = ciudad;
        this.tarjeta_credito = tarjeta_credito;
        this.mes = mes;
        this.anio = anio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPais() {
        return pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getTarjeta_credito() {
        return tarjeta_credito;
    }

    public String getMes() {
        return mes;
    }

    public String getAnio() {
        return anio;
    }
}
